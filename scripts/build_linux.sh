#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

export DIR
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

export PATH="$HOME/.local/bin:$PATH"

scons platform=linuxbsd tools=yes target=debug -j2 \
      udev=yes use_static_cpp=yes \
      CC="gcc-9" CXX="g++-9" progress=no warnings=no werror=no \
      disable_3d=yes disable_advanced_gui=yes tools=no
