#!/usr/bin/env bash
export DIR
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

apt-get update -qq
apt-get install -qqq software-properties-common
add-apt-repository -y ppa:ubuntu-toolchain-r/test
apt-get update -qq

apt-get install -qqq git cmake zip unzip build-essential pkg-config \
    libx11-dev libxcursor-dev libxinerama-dev libgl1-mesa-dev libcairo2 \
    libglu-dev libasound2-dev libpulse-dev libfreetype6-dev \
    libssl-dev libudev-dev libxrandr-dev libxi-dev curl \
    gcc-9 g++-9 python3-pip
pip3 install --user scons

sed -i "s:#! /usr/bin/env python:#! /usr/bin/python3:" "$HOME/.local/bin/scons"

git clone --depth 1 --branch "master" "https://github.com/godotengine/godot.git"
mkdir -p "$CI_PROJECT_DIR/artifacts"
