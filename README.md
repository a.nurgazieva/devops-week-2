# Dear Teacher, 

I don't find a reply for this question regarding possibility to make CI pipeline for another project (https://www.coursera.org/learn/devops/discussions/groups/8aD4I3heEeytPApSSmg3KQ),  so I would like to propose another CI pipeline -
#### https://gitlab.com/a.nurgazieva/devops-mds

I was really struggling with the build stage of GODOT since I have no prior experience with C++ compillation as well as any acquaintance with CI concepts (expect few short videos in the 2nd week of this course). I did not continue to try further, because I have no idea of what is happening under the hood (kind of frustrating experience).

I ended up creating another repository (just a simple project made with one of front-end frameworks) and I kindly ask you, if it's possible, to review CI for this repo - https://gitlab.com/a.nurgazieva/devops-mds/-/blob/main/.gitlab-ci.yml  I created three stages (deployment to GitLab pages - https://a.nurgazieva.gitlab.io/devops-mds/), the most inportant thing is that I clearly understand all of them, how this type of applications are built, tested and deployed. 
